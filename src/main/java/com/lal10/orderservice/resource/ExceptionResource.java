package com.lal10.orderservice.resource;

import com.lal10.orderservice.exception.ApiException;
import org.springframework.boot.context.config.ConfigDataNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionResource {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleRequestException(ConstraintViolationException e) {

        List<String> errorMessages = e.getConstraintViolations().stream().map(
                v -> v.getMessageTemplate()
        ).collect(Collectors.toList());


        ApiException apiException= new ApiException(
                errorMessages.get(0),
                HttpStatus.BAD_REQUEST,
                ZonedDateTime.now(ZoneId.of("Z"))
        );

        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleRequestException(MethodArgumentNotValidException e) {

        List<String> errorMessages = e.getBindingResult()
         .getAllErrors().stream().map(v -> v.getDefaultMessage())
                .collect(Collectors.toList());

        ApiException apiException= new ApiException(
                errorMessages.get(0),
                HttpStatus.BAD_REQUEST,
                ZonedDateTime.now(ZoneId.of("Z"))
        );

        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler
    public ResponseEntity<Object> handleNoContentRequest(ApiException e){

        ApiException apiException = new ApiException(
                e.getMessage(),
                e.getStatus(),
                e.getTimeStamp().withZoneSameLocal(ZoneId.of("Z"))
        );

        return new ResponseEntity<>(apiException, HttpStatus.NO_CONTENT);
    }
}
