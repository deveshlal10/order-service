package com.lal10.orderservice.resource;

import com.lal10.orderservice.dto.OrderRequest;
import com.lal10.orderservice.exception.ApiException;
import com.lal10.orderservice.model.Order;
import com.lal10.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderResource {

    @Autowired
    OrderService orderService;

    @GetMapping("/{id}")
    public Optional<Order> getOrder(@PathVariable Long id) {
        return orderService.orderRepository.findById(id);
    }

    @PostMapping
    @Transactional
    public OrderRequest saveOrder(@RequestBody @Valid OrderRequest request) {
        return orderService.saveOrder(request);
    }

    @PutMapping
    public Order updateOrder(@RequestBody @Valid Order order) throws ApiException {
        return orderService.updateOrder(order);
    }
}
