package com.lal10.orderservice.exception;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
import java.util.List;

public class ApiException extends RuntimeException {

    private final String message;
    private final HttpStatus status;
    private final ZonedDateTime timeStamp;

    public ApiException(String message, HttpStatus status, ZonedDateTime timeStamp) {
        this.message = message;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ZonedDateTime getTimeStamp() {
        return timeStamp;
    }
}
