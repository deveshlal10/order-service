package com.lal10.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "address")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;

    @Column(name = "address_line_1")
    @NotBlank(message = "address is required")
    private String addressLine1;

    @Column(name = "address_line_2")
    @NotBlank(message = "address is required")
    private String addressLine2;

    @Column(name = "address_type")
    @Enumerated(EnumType.STRING)
    private AddressType addressType;

    @Column(name = "alternate_no")
    @NotBlank(message = "alternate no is required")
    private String alterNateNo;

    @Column(name = "city")
    @NotBlank(message = "city is required")
    private String city;

    @Column(name = "created_at")
    private LocalDateTime creationTimestamp;

    @Column(name = "updated_at")
    private LocalDateTime updatedTimestamp;

    @Column(name = "customer_name")
    @NotBlank(message = "customer name is required")
    private String customerName;

    @Column(name = "default_address")
    @NotNull(message = "default address is required")
    private Boolean defaultAddress;

    @Column(name = "is_current")
    @NotNull
    private Boolean isCurrent;

    @Column(name = "landmark")
    @NotBlank(message = "landmark is required")
    private String landmark;

    @Column(name = "latitude")
    private Number latitude;

    @Column(name = "locality")
    private String locality;

    @Column(name = "longitude")
    private Number longitude;

    @Column(name = "mobile_no")
    @NotNull(message = "mobile no is required")
    private Long mobileNo;

    @Column(name = "state")
    @NotBlank(message = "state is required")
    private String state;

    @Column(name = "stateId")
    @NotBlank(message = "state id is required")
    private String stateId;

    @Column(name = "user_type")
    @NotBlank(message = "user type is required")
    private String userType;

    @Column(name = "zip_code")
    @NotBlank(message = "zip code is required")
    private String zipCode;
}
