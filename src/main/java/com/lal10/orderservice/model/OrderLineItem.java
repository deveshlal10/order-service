package com.lal10.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "order_line_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderLineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "discount", columnDefinition = "integer default 0")
    @NotNull
    private Integer discount;

    @Column(name = "final_cost")
    @NotNull(message = "final cost is required")
    private Integer finalCost;

    @Column(name = "tax")
    @NotNull
    private Integer tax;

    @Column(name = "total_cost")
    @NotNull(message = "total cost is required")
    private Integer totalCost;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "orderLineItem")
    @JoinColumn(name = "id")
    private LineItem lineItem;
}
