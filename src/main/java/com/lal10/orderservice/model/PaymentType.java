package com.lal10.orderservice.model;

public enum PaymentType {
    COD,
    CARD,
    ONLINE_BANKING
}
