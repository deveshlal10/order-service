package com.lal10.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "line_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "cost")
    @NotNull(message = "cost is required")
    private Integer cost;

    @Column(name = "discount")
    @NotNull(message = "discount is required")
    private Integer discount;

    @Column(name = "product_name")
    @NotBlank(message = "product name is required")
    private String productName;

    @Column(name = "product_id")
    @NotNull(message = "product id is required")
    private Integer productId;

    @Column(name = "quantity")
    @NotNull(message = "quantity is required")
    private Integer quantity;

    @Column(name = "sku_id")
    @NotNull(message = "sku id is required")
    private Integer skuId;

    @Column(name = "tax")
    @NotNull(message = "tax amount is required")
    private Integer tax;

    @Column(name = "total_cost")
    @NotNull(message = "total cost is required")
    private Integer totalCost;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_line_item_id", nullable = false)
    @JsonIgnore
    private OrderLineItem orderLineItem;
}
