package com.lal10.orderservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "customer_id")
    @NotNull(message = "customer id cannot be null")
    private Integer customerId;

    @Column(name = "merchant_id")
    @NotNull(message = "merchant id is required")
    private Integer merchantId;

    @Column(name = "address_id")
    private Integer addressId;

    @Column(name = "unique_id")
    @NotNull(message = "unique id is required")
    private Integer uniqueId;

    @Column(name = "user_id")
    @NotNull(message = "user id is required")
    private Integer userId;

    @Column(name = "is_active", columnDefinition = "int default 1")
    private Integer isActive;

    @Column(name = "shop_id")
    @NotNull(message = "shop id is required")
    private Integer shopId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatusType status;

    @Column(name = "created_at")
    private LocalDateTime currentTimeStamp;

    @Column(name = "updated_at")
    private LocalDateTime updatedTimeStamp;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "order")
    @JoinColumn(name = "id")
    private Address address;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    private List<OrderLineItem> orderLineItem;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "order")
    @JoinColumn(name = "id")
    private Payment payment;
}
