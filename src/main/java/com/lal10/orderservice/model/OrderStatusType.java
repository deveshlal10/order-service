package com.lal10.orderservice.model;

public enum OrderStatusType {
    PENDING,
    CANCELLED,
    DELIVERED
}
