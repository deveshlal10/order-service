package com.lal10.orderservice.model;

public enum AddressType {
    BILLING,
    SHIPPING
}
