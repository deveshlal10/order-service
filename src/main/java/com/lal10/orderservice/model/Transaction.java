package com.lal10.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payment_id", nullable = false)
    @JsonIgnore
    private Payment payment;

    @Column(name = "gateway_response")
    @NotBlank
    private String gatewayResponse;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "order_payment_id")
    @NotBlank(message = "order payment is is required")
    private String orderPaymentId;

    @Column(name = "success")
    @NotBlank
    private String success;

    @Column(name = "transaction_amount")
    @NotNull(message = "transaction amount is required")
    private Integer transactionAmount;

    @Column(name = "transaction_date")
    private LocalDateTime transactionDate;
}
