package com.lal10.orderservice.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Table(name = "payment")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "razorpay_order_id")
    @NotNull
    private Long razorpayOrderId;

    @Column(name = "razorpay_payment_id")
    @NotNull
    private Long razorpayPaymentId;

    @Column(name = "amount")
    @NotNull(message = "amount is required")
    private Integer amount;

    @Column(name = "gateway_type", columnDefinition = "varchar(10) default 'RazorPay'")
    private Integer gatewayType;

    @Column(name = "payment_date")
    private LocalDateTime creationTimestamp;

    @Column(name = "payment_type")
    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    @Column(name = "reference_number")
    @NotBlank
    private String referenceNumber;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "payment")
    @JoinColumn(name = "id")
    private Transaction transaction;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;
}
