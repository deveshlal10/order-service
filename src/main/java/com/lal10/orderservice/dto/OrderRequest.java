package com.lal10.orderservice.dto;

import com.lal10.orderservice.model.*;
import lombok.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.Entity;
import javax.validation.Valid;
import java.util.List;

@Data
@Valid
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {

    @Valid
    public Order order;
    @Valid
    public Address address;
    @Valid
    public OrderLineItem orderLineItem;
    @Valid
    public List<LineItem> lineItem;
    @Valid
    public Payment payment;
    @Valid
    public Transaction transaction;
}
