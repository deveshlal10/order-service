package com.lal10.orderservice.repository;

import com.lal10.orderservice.model.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineItemRepository extends JpaRepository<LineItem, Long> {

}
