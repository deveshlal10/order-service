package com.lal10.orderservice.service;


import com.lal10.orderservice.dto.OrderRequest;
import com.lal10.orderservice.exception.ApiException;
import com.lal10.orderservice.model.*;
import com.lal10.orderservice.repository.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EnumType;
import javax.transaction.Transactional;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    public OrderRepository orderRepository;
    @Autowired
    public AddressRepository addressRepository;
    @Autowired
    public OrderLineItemRepository orderLineItemRepository;
    @Autowired
    public LineItemRepository lineItemRepository;
    @Autowired
    public PaymentRepository paymentRepository;
    @Autowired
    public TransactionRepository transactionRepository;

    public OrderRequest saveOrder(OrderRequest request) {

        Order order = request.getOrder();
        order.setCurrentTimeStamp(LocalDateTime.now(Clock.systemUTC()));
        order.setUpdatedTimeStamp(LocalDateTime.now(Clock.systemUTC()));
        orderRepository.save(order);

        Address address = request.getAddress();
        address.setOrder(order);
        address.setCreationTimestamp(LocalDateTime.now(Clock.systemUTC()));
        order.setUpdatedTimeStamp(LocalDateTime.now(Clock.systemUTC()));
        addressRepository.save(address);

        OrderLineItem orderLineItem = request.getOrderLineItem();
        orderLineItem.setOrder(order);
        orderLineItemRepository.save(orderLineItem);

        List<LineItem> lineItems = new ArrayList<>();
        LineItem lineItemAdded;
        for(int i=0; i<request.getLineItem().size(); i++) {
            LineItem lineItem = request.getLineItem().get(i);
            lineItem.setOrderLineItem(orderLineItem);
            lineItemAdded = lineItemRepository.save(lineItem);
            lineItems.add(lineItemAdded);
        }

        Payment payment = request.getPayment();
        payment.setCreationTimestamp(LocalDateTime.now(Clock.systemUTC()));
        payment.setOrder(order);
        paymentRepository.save(payment);

        Transaction transaction = request.getTransaction();
        transaction.setTransactionDate(LocalDateTime.now(Clock.systemUTC()));
        transaction.setPayment(payment);
        transactionRepository.save(transaction);

        return new OrderRequest(order, address, orderLineItem, lineItems, payment, transaction);
    }

    public Order updateOrder(Order order) throws ApiException {
        Order orderFound = null;
        Optional<Order> optionalOrder = orderRepository.findById(order.getId());
        if (optionalOrder.isPresent()){
            orderFound = optionalOrder.get();
            if (StringUtils.isNotBlank(order.getStatus().toString())){
                orderFound.setStatus(order.getStatus());
            }
            orderFound.setUpdatedTimeStamp(LocalDateTime.now(Clock.systemUTC()));
            return orderRepository.save(orderFound);
        }else{
            throw new ApiException(
                    "Order not found for id " + order.getId(),
                    HttpStatus.NO_CONTENT,
                    ZonedDateTime.now(ZoneId.of("Z"))
            );
        }
    }
}
